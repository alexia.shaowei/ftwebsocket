package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"text/template"

	websocket "sw.websocket/wsx"
)

func main() {
	fmt.Println("main::main() -> init")

	http.HandleFunc("/ws", ws)
	http.HandleFunc("/index", index)

	if err := http.ListenAndServe(":8800", nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}

}

func ws(w http.ResponseWriter, r *http.Request) {
	c, err := websocket.New(w, r, nil)
	if err != nil {
		fmt.Printf("new error: %v\n", err)
		return
	}
	fmt.Println("New OK ------------------------------------------")

	defer func() {
		err = c.Close(websocket.IANAStatusCodeNormalClosure, "defer close")
		if err != nil {
			fmt.Printf("what the fuck error: %v\n", err)
		}
	}()

	fmt.Printf("connected: %v\n", &c)

	count := 0
	for {
		msgT, bt, err := c.Read(context.TODO())
		fmt.Println("Read loop OK ------------------------------------------")
		if err != nil {
			fmt.Printf("message receive loop error: %v\n", err)
			break
		}
		fmt.Printf("msgT: %v, bt: %v\n", msgT, string(bt))

		rtn := fmt.Sprintf("pointer: %v, count: %d", &c, count)

		if err := c.Write(context.TODO(), msgT, []byte(rtn)); err != nil {
			fmt.Printf("message callback loop error: %v\n", err)
			break
		}

		count++
	}

	fmt.Println("request done")
}

func index(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("index.html")
	t.Execute(w, nil)
}
