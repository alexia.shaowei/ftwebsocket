package ftwebsocket

import (
	"bufio"
	"context"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"io"
	"sync"
	"time"

	werr "sw.websocket/wsx/internal/wraperror"
)

func (c *Connector) Writer(ctx context.Context, typ MessageType) (io.WriteCloser, error) {
	w, err := c.writer(ctx, typ)
	if err != nil {
		return nil, fmt.Errorf("failed to get writer: %w", err)
	}
	return w, nil
}
func (c *Connector) Write(ctx context.Context, typ MessageType, p []byte) error {
	_, err := c.write(ctx, typ, p)
	if err != nil {
		return fmt.Errorf("failed to write msg: %w", err)
	}

	return nil
}

func (c *Connector) writer(ctx context.Context, typ MessageType) (io.WriteCloser, error) {
	err := c.msgWriterState.reset(ctx, typ)
	if err != nil {
		return nil, err
	}

	return &msgWriter{
		mw:     c.msgWriterState,
		closed: false,
	}, nil
}

func (c *Connector) write(ctx context.Context, typ MessageType, p []byte) (int, error) {
	mw, err := c.writer(ctx, typ)
	if err != nil {
		return 0, err
	}

	if !c.flate() {
		defer c.msgWriterState.mu.unlock()
		return c.writeFrame(ctx, true, false, c.msgWriterState.opcode, p)
	}

	n, err := mw.Write(p)
	if err != nil {
		return n, err
	}

	err = mw.Close()
	return n, err
}

func (c *Connector) writeControl(ctx context.Context, opcode opcode, p []byte) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	_, err := c.writeFrame(ctx, true, false, opcode, p)
	if err != nil {
		return fmt.Errorf("failed to write control frame %v: %w", opcode, err)
	}

	return nil
}

func (c *Connector) writeFrame(ctx context.Context, fin bool, flate bool, opcode opcode, p []byte) (_ int, err error) {
	err = c.writeFrameMu.lock(ctx)
	if err != nil {
		return 0, err
	}
	defer c.writeFrameMu.unlock()

	c.closeMu.Lock()
	wroteClose := c.wroteClose
	c.closeMu.Unlock()
	if wroteClose && opcode != opCloseFrame {
		select {
		case <-ctx.Done():
			return 0, ctx.Err()

		case <-c.closed:
			return 0, c.closeErr

		}
	}

	select {
	case <-c.closed:
		return 0, c.closeErr

	case c.writeTimeout <- ctx:
	}

	defer func() {
		if err != nil {
			select {
			case <-c.closed:
				err = c.closeErr

			case <-ctx.Done():
				err = ctx.Err()

			}

			c.close(err)
			err = fmt.Errorf("failed to write frame: %w", err)
		}
	}()

	c.writeHeader.framefin = fin
	c.writeHeader.opcode = opcode
	c.writeHeader.payloadLength = int64(len(p))

	if c.client {
		c.writeHeader.masked = true
		_, err = io.ReadFull(rand.Reader, c.writeHeaderBuf[:4])
		if err != nil {
			return 0, fmt.Errorf("failed to generate masking key: %w", err)
		}

		c.writeHeader.maskKey = binary.LittleEndian.Uint32(c.writeHeaderBuf[:])
	}

	c.writeHeader.framersv1 = false
	if flate && (opcode == opTextFrame || opcode == opBinaryFrame) {
		c.writeHeader.framersv1 = true
	}

	err = writeFrameHeader(c.writeHeader, c.bufioW, c.writeHeaderBuf[:])
	if err != nil {
		return 0, err
	}

	n, err := c.writeFramePayload(p)
	if err != nil {
		return n, err
	}

	if c.writeHeader.framefin {
		err = c.bufioW.Flush()
		if err != nil {
			return n, fmt.Errorf("failed to flush: %w", err)
		}
	}

	select {
	case <-c.closed:
		return n, c.closeErr

	case c.writeTimeout <- context.Background():

		// default:
	}

	return n, nil
}

func (c *Connector) writeFramePayload(p []byte) (n int, err error) {
	defer werr.ErrWrap(&err, "failed to write frame payload")

	if !c.writeHeader.masked {
		return c.bufioW.Write(p)
	}

	maskKey := c.writeHeader.maskKey
	for len(p) > 0 {
		if c.bufioW.Available() == 0 {
			err = c.bufioW.Flush()
			if err != nil {
				return n, err
			}
		}

		i := c.bufioW.Buffered()

		j := len(p)
		if j > c.bufioW.Available() {
			j = c.bufioW.Available()
		}

		_, err := c.bufioW.Write(p[:j])
		if err != nil {
			return n, err
		}

		maskKey = maskRFC6455Sec53(maskKey, c.writeBuf[i:c.bufioW.Buffered()])

		p = p[j:]
		n += j
	}

	return n, nil
}

type writerFunc func(p []byte) (int, error)

func (f writerFunc) Write(p []byte) (int, error) {
	fmt.Println("iowrite.go:::Write")
	return f(p)
}

func extractBufioWriterBuf(bw *bufio.Writer, w io.Writer) []byte {
	fmt.Println("iowrite.go:::extractBufioWriterBuf")
	var writeBuf []byte
	bw.Reset(writerFunc(func(p2 []byte) (int, error) {
		writeBuf = p2[:cap(p2)]
		return len(p2), nil
	}))

	bw.WriteByte(0)
	bw.Flush()

	bw.Reset(w)

	return writeBuf
}

func (c *Connector) writeError(code StatusCode, err error) {
	fmt.Println("iowrite.go:::writeError")
	c.setCloseErr(err)
	c.writeClose(code, err.Error())
	c.close(nil)
}

var bufioWriterPool sync.Pool

func putBufioWriter(bw *bufio.Writer) {
	fmt.Println("Dial.go:::putBufioWriter")
	bufioWriterPool.Put(bw)
}
