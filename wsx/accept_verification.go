package ftwebsocket

import (
	"crypto/sha1"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"net/textproto"
	"net/url"
	"strings"
)

func verifyReq(w http.ResponseWriter, r *http.Request) (errCode int, _ error) {
	if !r.ProtoAtLeast(1, 1) {
		return http.StatusUpgradeRequired, fmt.Errorf("WebSocket err: request must be at least HTTP/1.1: %q", r.Proto)
	}

	if !headerContainsTokenIgnoreCase(r.Header, "Connection", "Upgrade") {
		w.Header().Set("Connection", "Upgrade")
		w.Header().Set("Upgrade", "websocket")

		return http.StatusUpgradeRequired, fmt.Errorf("WebSocket err: header %q does not contain Upgrade", r.Header.Get("Connection"))
	}

	if !headerContainsTokenIgnoreCase(r.Header, "Upgrade", "websocket") {
		w.Header().Set("Connection", "Upgrade")
		w.Header().Set("Upgrade", "websocket")

		return http.StatusUpgradeRequired, fmt.Errorf("WebSocket err: Upgrade header %q does not contain websocket", r.Header.Get("Upgrade"))
	}

	if r.Method != "GET" {
		return http.StatusMethodNotAllowed, fmt.Errorf("WebSocket err: request method is not GET but %q", r.Method)
	}

	if r.Header.Get("Sec-WebSocket-Version") != "13" {
		w.Header().Set("Sec-WebSocket-Version", "13")

		return http.StatusBadRequest, fmt.Errorf("unsupported WebSocket protocol version (only 13 is supported): %q", r.Header.Get("Sec-WebSocket-Version"))
	}

	if r.Header.Get("Sec-WebSocket-Key") == "" {
		return http.StatusBadRequest, errors.New("WebSocket protocol violation: missing Sec-WebSocket-Key")
	}

	return 0, nil
}

func headerTokens(h http.Header, key string) []string {
	key = textproto.CanonicalMIMEHeaderKey(key)

	var tokens []string
	for _, val := range h[key] {

		val = strings.TrimSpace(val)
		for _, token := range strings.Split(val, ",") {
			token = strings.TrimSpace(token)
			tokens = append(tokens, token)
		}
	}

	return tokens
}

func headerContainsTokenIgnoreCase(h http.Header, key, token string) bool {
	for _, val := range headerTokens(h, key) {
		if strings.EqualFold(val, token) {
			return true
		}
	}

	return false
}

// exclude localhost | filepath
func authenticateOrigin(r *http.Request, originHosts []string) error {
	origin := r.Header.Get("Origin")
	if origin == "" {
		return nil
	}

	u, err := url.Parse(origin)
	if err != nil {
		return fmt.Errorf("failed to parse Origin header %q: %w", origin, err)
	}

	if strings.EqualFold(r.Host, u.Host) {
		return nil
	}

	for _, pattern := range originHosts {

		matched, err := match(pattern, u.Host)
		if err != nil {
			return fmt.Errorf("failed to parse filepath pattern %q: %w", pattern, err)
		}

		if matched {
			return nil
		}
	}

	return fmt.Errorf("request Origin %q is not authorized for Host %q", origin, r.Host)
}

func secWebSocketAccept(secWebSocketKey string) string {
	h := sha1.New()
	h.Write([]byte(secWebSocketKey))
	h.Write([]byte("258EAFA5-E914-47DA-95CA-C5AB0DC85B11")) //websocket magic string

	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

func querySubprotocols(r *http.Request, subprotocols []string) string {
	htks := headerTokens(r.Header, "Sec-WebSocket-Protocol")

	for _, subprotocol := range subprotocols {
		for _, cp := range htks {
			if strings.EqualFold(subprotocol, cp) {
				return cp
			}
		}
	}

	return ""
}
