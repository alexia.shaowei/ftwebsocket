package ftwebsocket

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"math"
	"math/bits"

	werr "sw.websocket/wsx/internal/wraperror"
)

type opcode int

// rfc6455#section-11.8.
const (
	opContinuationFrame opcode = 0
	opTextFrame         opcode = 1
	opBinaryFrame       opcode = 2
	opCloseFrame        opcode = 8
	opPingFrame         opcode = 9
	opPongFrame         opcode = 10
)

// /rfc6455#section-5.5.
const maxPayload = 125

// rfc6455#section-5.2.
type header struct {
	framefin  bool
	framersv1 bool
	framersv2 bool
	framersv3 bool

	opcode opcode

	payloadLength int64

	masked  bool
	maskKey uint32
}

func readFrameHeaderRFC6455Sec52(r *bufio.Reader, buf []byte) (h header, err error) {
	defer werr.ErrWrap(&err, "failed to read frame header")

	b, err := r.ReadByte()
	if err != nil {
		return header{}, err
	}

	h.framefin = b&(1<<7) != 0
	h.framersv1 = b&(1<<6) != 0
	h.framersv2 = b&(1<<5) != 0
	h.framersv3 = b&(1<<4) != 0

	h.opcode = opcode(b & 0xf)

	b, err = r.ReadByte()
	if err != nil {
		return header{}, err
	}

	h.masked = b&(1<<7) != 0

	payloadLength := b &^ (1 << 7)
	switch {
	case payloadLength < 126:
		h.payloadLength = int64(payloadLength)

	case payloadLength == 126:
		_, err = io.ReadFull(r, buf[:2])
		h.payloadLength = int64(binary.BigEndian.Uint16(buf))

	case payloadLength == 127:
		_, err = io.ReadFull(r, buf)
		h.payloadLength = int64(binary.BigEndian.Uint64(buf))

	}

	if err != nil {
		return header{}, err
	}

	if h.payloadLength < 0 {
		return header{}, fmt.Errorf("received negative payload length: %v", h.payloadLength)
	}

	if h.masked {
		_, err = io.ReadFull(r, buf[:4])
		if err != nil {
			return header{}, err
		}

		h.maskKey = binary.LittleEndian.Uint32(buf)
	}

	return h, nil
}

func writeFrameHeader(h header, w *bufio.Writer, buf []byte) (err error) {
	defer werr.ErrWrap(&err, "failed to write frame header")

	var b byte
	if h.framefin {
		b |= 1 << 7
	}

	if h.framersv1 {
		b |= 1 << 6
	}

	if h.framersv2 {
		b |= 1 << 5
	}

	if h.framersv3 {
		b |= 1 << 4
	}

	b |= byte(h.opcode)

	err = w.WriteByte(b)
	if err != nil {
		return err
	}

	lengthByte := byte(0)
	if h.masked {
		lengthByte |= 1 << 7
	}

	switch {
	case h.payloadLength > math.MaxUint16:
		lengthByte |= 127

	case h.payloadLength > 125:
		lengthByte |= 126

	case h.payloadLength >= 0:
		lengthByte |= byte(h.payloadLength)
	}

	err = w.WriteByte(lengthByte)
	if err != nil {
		return err
	}

	switch {
	case h.payloadLength > math.MaxUint16:
		binary.BigEndian.PutUint64(buf, uint64(h.payloadLength))
		_, err = w.Write(buf)

	case h.payloadLength > 125:
		binary.BigEndian.PutUint16(buf, uint16(h.payloadLength))
		_, err = w.Write(buf[:2])

	}

	if err != nil {
		return err
	}

	if h.masked {
		binary.LittleEndian.PutUint32(buf, h.maskKey)

		_, err = w.Write(buf[:4])
		if err != nil {
			return err
		}
	}

	return nil
}

func maskRFC6455Sec53(key uint32, bt []byte) uint32 {
	if len(bt) >= 8 {
		k := uint64(key)<<32 | uint64(key)

		for len(bt) >= 128 {
			v := binary.LittleEndian.Uint64(bt)

			binary.LittleEndian.PutUint64(bt, v^k)
			v = binary.LittleEndian.Uint64(bt[8:16])

			binary.LittleEndian.PutUint64(bt[8:16], v^k)
			v = binary.LittleEndian.Uint64(bt[16:24])

			binary.LittleEndian.PutUint64(bt[16:24], v^k)
			v = binary.LittleEndian.Uint64(bt[24:32])

			binary.LittleEndian.PutUint64(bt[24:32], v^k)
			v = binary.LittleEndian.Uint64(bt[32:40])

			binary.LittleEndian.PutUint64(bt[32:40], v^k)
			v = binary.LittleEndian.Uint64(bt[40:48])

			binary.LittleEndian.PutUint64(bt[40:48], v^k)
			v = binary.LittleEndian.Uint64(bt[48:56])

			binary.LittleEndian.PutUint64(bt[48:56], v^k)
			v = binary.LittleEndian.Uint64(bt[56:64])

			binary.LittleEndian.PutUint64(bt[56:64], v^k)
			v = binary.LittleEndian.Uint64(bt[64:72])

			binary.LittleEndian.PutUint64(bt[64:72], v^k)
			v = binary.LittleEndian.Uint64(bt[72:80])

			binary.LittleEndian.PutUint64(bt[72:80], v^k)
			v = binary.LittleEndian.Uint64(bt[80:88])

			binary.LittleEndian.PutUint64(bt[80:88], v^k)
			v = binary.LittleEndian.Uint64(bt[88:96])

			binary.LittleEndian.PutUint64(bt[88:96], v^k)
			v = binary.LittleEndian.Uint64(bt[96:104])

			binary.LittleEndian.PutUint64(bt[96:104], v^k)
			v = binary.LittleEndian.Uint64(bt[104:112])

			binary.LittleEndian.PutUint64(bt[104:112], v^k)
			v = binary.LittleEndian.Uint64(bt[112:120])

			binary.LittleEndian.PutUint64(bt[112:120], v^k)
			v = binary.LittleEndian.Uint64(bt[120:128])

			binary.LittleEndian.PutUint64(bt[120:128], v^k)

			bt = bt[128:]
		}

		for len(bt) >= 64 {
			v := binary.LittleEndian.Uint64(bt)

			binary.LittleEndian.PutUint64(bt, v^k)
			v = binary.LittleEndian.Uint64(bt[8:16])

			binary.LittleEndian.PutUint64(bt[8:16], v^k)
			v = binary.LittleEndian.Uint64(bt[16:24])

			binary.LittleEndian.PutUint64(bt[16:24], v^k)
			v = binary.LittleEndian.Uint64(bt[24:32])

			binary.LittleEndian.PutUint64(bt[24:32], v^k)
			v = binary.LittleEndian.Uint64(bt[32:40])

			binary.LittleEndian.PutUint64(bt[32:40], v^k)
			v = binary.LittleEndian.Uint64(bt[40:48])

			binary.LittleEndian.PutUint64(bt[40:48], v^k)
			v = binary.LittleEndian.Uint64(bt[48:56])

			binary.LittleEndian.PutUint64(bt[48:56], v^k)
			v = binary.LittleEndian.Uint64(bt[56:64])

			binary.LittleEndian.PutUint64(bt[56:64], v^k)

			bt = bt[64:]
		}

		for len(bt) >= 32 {
			v := binary.LittleEndian.Uint64(bt)

			binary.LittleEndian.PutUint64(bt, v^k)
			v = binary.LittleEndian.Uint64(bt[8:16])

			binary.LittleEndian.PutUint64(bt[8:16], v^k)
			v = binary.LittleEndian.Uint64(bt[16:24])

			binary.LittleEndian.PutUint64(bt[16:24], v^k)
			v = binary.LittleEndian.Uint64(bt[24:32])

			binary.LittleEndian.PutUint64(bt[24:32], v^k)

			bt = bt[32:]
		}

		for len(bt) >= 16 {
			v := binary.LittleEndian.Uint64(bt)

			binary.LittleEndian.PutUint64(bt, v^k)
			v = binary.LittleEndian.Uint64(bt[8:16])

			binary.LittleEndian.PutUint64(bt[8:16], v^k)

			bt = bt[16:]
		}

		for len(bt) >= 8 {
			v := binary.LittleEndian.Uint64(bt)

			binary.LittleEndian.PutUint64(bt, v^k)
			bt = bt[8:]
		}
	}

	for len(bt) >= 4 {
		v := binary.LittleEndian.Uint32(bt)

		binary.LittleEndian.PutUint32(bt, v^key)
		bt = bt[4:]
	}

	for idx := range bt {
		bt[idx] ^= byte(key)
		key = bits.RotateLeft32(key, -8)
	}

	return key
}
