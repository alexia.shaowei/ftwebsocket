package ftwebsocket

import (
	"compress/flate"
	"io"
	"net/http"
	"sync"
)

// rfc7692
type CompressionMode int

const (
	CompNoContextTakeover CompressionMode = 0
	CompContextTakeover   CompressionMode = 1
	CompDisabled          CompressionMode = 2
)

type compressionOptions struct {
	clientNoContextTakeover bool
	serverNoContextTakeover bool
}

func (ref CompressionMode) options() *compressionOptions {

	return &compressionOptions{
		clientNoContextTakeover: ref == CompNoContextTakeover,
		serverNoContextTakeover: ref == CompNoContextTakeover,
	}
}

func (ref *compressionOptions) setHeader(h http.Header) {
	val := "permessage-deflate"
	if ref.clientNoContextTakeover {
		val += "; client_no_context_takeover"
	}

	if ref.serverNoContextTakeover {
		val += "; server_no_context_takeover"
	}

	h.Set("Sec-WebSocket-Extensions", val)
}

const deflateMsgTail = "\x00\x00\xff\xff"

type trimLastFourBytesWriter struct {
	iow  io.Writer
	tail []byte
}

func (tw *trimLastFourBytesWriter) reset() {
	if tw != nil && tw.tail != nil {
		tw.tail = tw.tail[:0]
	}
}

func (tw *trimLastFourBytesWriter) Write(p []byte) (int, error) {
	if tw.tail == nil {
		tw.tail = make([]byte, 0, 4)
	}

	extra := len(tw.tail) + len(p) - 4

	if extra <= 0 {
		tw.tail = append(tw.tail, p...)
		return len(p), nil
	}

	if extra > len(tw.tail) {
		extra = len(tw.tail)
	}

	if extra > 0 {
		_, err := tw.iow.Write(tw.tail[:extra])
		if err != nil {
			return 0, err
		}

		n := copy(tw.tail, tw.tail[extra:])
		tw.tail = tw.tail[:n]
	}

	if len(p) <= 4 {
		tw.tail = append(tw.tail, p...)
		return len(p), nil
	}

	tw.tail = append(tw.tail, p[len(p)-4:]...)

	p = p[:len(p)-4]
	n, err := tw.iow.Write(p)

	return n + 4, err
}

var flateReaderPool sync.Pool

func getFlateReader(r io.Reader, dict []byte) io.Reader {
	fr, ok := flateReaderPool.Get().(io.Reader)
	if !ok {
		return flate.NewReaderDict(r, dict)
	}

	fr.(flate.Resetter).Reset(r, dict)

	return fr
}

func putFlateReader(fr io.Reader) {
	flateReaderPool.Put(fr)
}

type slidingWindow struct {
	buf []byte
}

var swPoolMu sync.RWMutex
var swPool = map[int]*sync.Pool{}

func slidingWindowPool(n int) *sync.Pool {
	swPoolMu.RLock()
	p, ok := swPool[n]
	swPoolMu.RUnlock()
	if ok {
		return p
	}

	p = &sync.Pool{}

	swPoolMu.Lock()
	swPool[n] = p
	swPoolMu.Unlock()

	return p
}

func (sw *slidingWindow) init(n int) {
	if sw.buf != nil {
		return
	}

	if n == 0 {
		n = 32768
	}

	pool := slidingWindowPool(n)
	buf, ok := pool.Get().([]byte)
	if ok {
		sw.buf = buf[:0]

	} else {
		sw.buf = make([]byte, 0, n)

	}
}

func (sw *slidingWindow) close() {
	if sw.buf == nil {
		return
	}

	swPoolMu.Lock()
	swPool[cap(sw.buf)].Put(&sw.buf)
	swPoolMu.Unlock()
	sw.buf = nil
}

func (sw *slidingWindow) write(p []byte) {
	if len(p) >= cap(sw.buf) {
		sw.buf = sw.buf[:cap(sw.buf)]
		p = p[len(p)-cap(sw.buf):]
		copy(sw.buf, p)
		return
	}

	left := cap(sw.buf) - len(sw.buf)
	if left < len(p) {
		spaceNeeded := len(p) - left
		copy(sw.buf, sw.buf[spaceNeeded:])
		sw.buf = sw.buf[:len(sw.buf)-spaceNeeded]
	}

	sw.buf = append(sw.buf, p...)
}
