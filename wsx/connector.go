package ftwebsocket

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"runtime"
	"strconv"
	"sync"
	"sync/atomic"
)

type MessageType int

// rfc6455#section-5.6
const (
	MessageText MessageType = iota + 1
	MessageBinary
)

type Connector struct {
	subprotocol       string
	ioRWCloser        io.ReadWriteCloser
	client            bool
	compressionopts   *compressionOptions
	flateThreshold    int
	bufioR            *bufio.Reader
	bufioW            *bufio.Writer
	readTimeout       chan context.Context
	writeTimeout      chan context.Context
	readMu            *connMux
	readHeaderBuf     [8]byte
	readControlBuf    [maxPayload]byte
	msgReader         *msgReader
	readCloseFrameErr error
	msgWriterState    *msgWriterState
	writeFrameMu      *connMux
	writeBuf          []byte
	writeHeaderBuf    [8]byte
	writeHeader       header
	closed            chan struct{}
	closeMu           sync.Mutex
	closeErr          error
	wroteClose        bool
	pingCounter       int32
	activePingsMu     sync.Mutex
	actPings          map[string]chan<- struct{}
}

type connCfg struct {
	subprotocol     string
	rwCloser        io.ReadWriteCloser
	client          bool
	compressionopts *compressionOptions
	flateThreshold  int
	bufR            *bufio.Reader
	bufW            *bufio.Writer
}

func newConn(cfg connCfg) *Connector {
	c := &Connector{
		subprotocol:     cfg.subprotocol,
		ioRWCloser:      cfg.rwCloser,
		client:          cfg.client,
		compressionopts: cfg.compressionopts,
		flateThreshold:  cfg.flateThreshold,
		bufioR:          cfg.bufR,
		bufioW:          cfg.bufW,
		readTimeout:     make(chan context.Context),
		writeTimeout:    make(chan context.Context),
		closed:          make(chan struct{}),
		actPings:        make(map[string]chan<- struct{}),
	}

	c.readMu = newConnMux(c)
	c.writeFrameMu = newConnMux(c)
	c.msgReader = newMsgReader(c)
	c.msgWriterState = newMsgWriterState(c)
	if c.client {
		c.writeBuf = extractBufioWriterBuf(c.bufioW, c.ioRWCloser)
	}

	if c.flate() && c.flateThreshold == 0 {
		c.flateThreshold = 128
		if !c.msgWriterState.flateContextTakeover() {
			c.flateThreshold = 512
		}
	}

	runtime.SetFinalizer(c, func(c *Connector) {
		c.close(errors.New("connection garbage collected"))
	})

	go c.timeoutLoop()

	return c
}

func (ref *Connector) close(err error) {
	ref.closeMu.Lock()
	defer ref.closeMu.Unlock()

	if ref.isClosed() {
		return
	}
	ref.setCloseErrLocked(err)
	close(ref.closed)

	runtime.SetFinalizer(ref, nil)

	ref.ioRWCloser.Close()

	go func() {
		ref.msgWriterState.close()
		ref.msgReader.close()
	}()
}

func (c *Connector) timeoutLoop() {

	readCtx := context.Background()
	writeCtx := context.Background()

	for {
		select {
		case <-c.closed:
			return

		case writeCtx = <-c.writeTimeout:

		case readCtx = <-c.readTimeout:

		case <-readCtx.Done():
			c.setCloseErr(fmt.Errorf("read timed out: %w", readCtx.Err()))

			go c.writeError(IANAStatusCodePolicyViolation, errors.New("timed out"))

		case <-writeCtx.Done():
			c.close(fmt.Errorf("write timed out: %w", writeCtx.Err()))

			return
		}
	}
}

func (ref *Connector) flate() bool {
	return ref.compressionopts != nil
}

func (ref *Connector) Ping(ctx context.Context) error {
	p := atomic.AddInt32(&ref.pingCounter, 1)

	err := ref.ping(ctx, strconv.Itoa(int(p)))
	if err != nil {
		return fmt.Errorf("failed to ping: %w", err)
	}

	return nil
}

func (ref *Connector) ping(ctx context.Context, p string) error {
	pong := make(chan struct{}, 1)

	ref.activePingsMu.Lock()
	ref.actPings[p] = pong
	ref.activePingsMu.Unlock()

	defer func() {
		ref.activePingsMu.Lock()
		delete(ref.actPings, p)
		ref.activePingsMu.Unlock()
	}()

	err := ref.writeControl(ctx, opPingFrame, []byte(p))
	if err != nil {
		return err
	}

	select {
	case <-ref.closed:
		return ref.closeErr

	case <-ctx.Done():
		err := fmt.Errorf("failed to wait for pong: %w", ctx.Err())
		ref.close(err)

		return err

	case <-pong:

		return nil
	}
}
