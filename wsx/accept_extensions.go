package ftwebsocket

import (
	"fmt"
	"net/http"
	"strings"
)

type websocketExtension struct {
	name   string
	params []string
}

func websocketExtensions(h http.Header) []websocketExtension {
	var extensions []websocketExtension

	htks := headerTokens(h, "Sec-WebSocket-Extensions")
	for _, htk := range htks {
		if htk == "" {
			continue
		}

		vals := strings.Split(htk, ";")
		for i := range vals {
			vals[i] = strings.TrimSpace(vals[i])
		}

		esExtension := websocketExtension{
			name:   vals[0],
			params: vals[1:],
		}

		extensions = append(extensions, esExtension)
	}

	return extensions
}

func acceptCompression(r *http.Request, w http.ResponseWriter, mode CompressionMode) (*compressionOptions, error) {
	if mode == CompDisabled {
		return nil, nil
	}

	for _, ext := range websocketExtensions(r.Header) {
		switch ext.name {
		case "permessage-deflate":
			return acceptDeflate(w, ext, mode)

		}
	}

	return nil, nil
}

func acceptDeflate(w http.ResponseWriter, ext websocketExtension, mode CompressionMode) (*compressionOptions, error) {
	compressionMode := mode.options()

	for _, param := range ext.params {
		switch param {
		case "client_no_context_takeover":
			compressionMode.clientNoContextTakeover = true
			continue

		case "server_no_context_takeover":
			compressionMode.serverNoContextTakeover = true
			continue
		}

		if strings.HasPrefix(param, "client_max_window_bits") {
			continue
		}

		err := fmt.Errorf("unsupported permessage-deflate parameter: %q", param)
		http.Error(w, err.Error(), http.StatusBadRequest)

		return nil, err
	}

	compressionMode.setHeader(w.Header())

	return compressionMode, nil
}
