package ftwebsocket

import (
	"fmt"
	"io"

	"sw.websocket/wsx/internal/ftsync"
)

const defaultReadLimit = 32768

type limitReader struct {
	conn  *Connector
	ior   io.Reader
	limit ftsync.Int64
	n     int64
}

func newLimitReader(c *Connector, r io.Reader, limit int64) *limitReader {
	limitR := &limitReader{
		conn: c,
	}

	limitR.limit.Store(limit)
	limitR.reset(r)

	return limitR
}

func (ref *limitReader) reset(r io.Reader) {
	ref.n = ref.limit.Load()
	ref.ior = r
}

func (ref *limitReader) Read(p []byte) (int, error) {
	if ref.n <= 0 {
		err := fmt.Errorf("read limited at %v bytes", ref.limit.Load())
		ref.conn.writeError(IANAStatusCodeMessageTooBig, err)

		return 0, err
	}

	if int64(len(p)) > ref.n {
		p = p[:ref.n]
	}

	n, err := ref.ior.Read(p)
	ref.n -= int64(n)

	return n, err
}
