package ftwebsocket

import (
	"context"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"time"

	werr "sw.websocket/wsx/internal/wraperror"
)

func (c *Connector) Reader(ctx context.Context) (MessageType, io.Reader, error) {
	return c.reader(ctx)
}

func (c *Connector) Read(ctx context.Context) (MessageType, []byte, error) {
	typ, r, err := c.Reader(ctx)
	if err != nil {
		return 0, nil, err
	}

	b, err := ioutil.ReadAll(r)

	return typ, b, err
}

func (c *Connector) CloseRead(ctx context.Context) context.Context {
	fmt.Println("ioread:::CloseRead")
	ctx, cancel := context.WithCancel(ctx)
	go func() {
		defer cancel()
		c.Reader(ctx)
		c.Close(IANAStatusCodePolicyViolation, "unexpected data message")
	}()
	return ctx
}

func (c *Connector) readRSV1Illegal(h header) bool {
	if !c.flate() {
		return true
	}

	if h.opcode != opTextFrame && h.opcode != opBinaryFrame {
		return true
	}
	return false
}

func (c *Connector) readLoop(ctx context.Context) (header, error) {
	for {
		h, err := c.readFrameHeader(ctx)
		if err != nil {
			return header{}, err
		}

		if h.framersv1 && c.readRSV1Illegal(h) || h.framersv2 || h.framersv3 {
			err := fmt.Errorf("received header with unexpected rsv bits set: %v:%v:%v", h.framersv1, h.framersv2, h.framersv3)
			c.writeError(IANAStatusCodeProtocolError, err)

			return header{}, err
		}

		if !c.client && !h.masked {
			return header{}, errors.New("received unmasked frame from client")
		}

		switch h.opcode {
		case opCloseFrame, opPingFrame, opPongFrame:
			err = c.handleControl(ctx, h)
			if err != nil {
				if h.opcode == opCloseFrame && CloseStatus(err) != -1 {
					return header{}, err
				}

				return header{}, fmt.Errorf("failed to handle control frame %v: %w", h.opcode, err)
			}

		case opContinuationFrame, opTextFrame, opBinaryFrame:
			return h, nil

		default:
			err := fmt.Errorf("received unknown opcode %v", h.opcode)
			c.writeError(IANAStatusCodeProtocolError, err)

			return header{}, err
		}
	}
}

func (c *Connector) readFrameHeader(ctx context.Context) (header, error) {
	select {
	case <-c.closed:
		return header{}, c.closeErr

	case c.readTimeout <- ctx:
	}

	h, err := readFrameHeaderRFC6455Sec52(c.bufioR, c.readHeaderBuf[:])
	if err != nil {
		select {
		case <-c.closed:
			return header{}, c.closeErr

		case <-ctx.Done():
			return header{}, ctx.Err()

		default:
			c.close(err)
			return header{}, err

		}
	}

	select {
	case <-c.closed:
		return header{}, c.closeErr

	case c.readTimeout <- context.Background():
	}

	return h, nil
}

func (c *Connector) readFramePayload(ctx context.Context, p []byte) (int, error) {
	fmt.Println("ioread:::readFramePayload")
	select {
	case <-c.closed:
		return 0, c.closeErr

	case c.readTimeout <- ctx:
	}

	n, err := io.ReadFull(c.bufioR, p)
	if err != nil {
		select {
		case <-c.closed:
			return n, c.closeErr

		case <-ctx.Done():
			return n, ctx.Err()

		default:
			err = fmt.Errorf("failed to read frame payload: %w", err)
			c.close(err)
			return n, err
		}
	}

	select {
	case <-c.closed:
		return n, c.closeErr

	case c.readTimeout <- context.Background():
	}

	return n, err
}

func (c *Connector) handleControl(ctx context.Context, h header) (err error) {
	if h.payloadLength < 0 || h.payloadLength > maxPayload {
		err := fmt.Errorf("received control frame payload with invalid length: %d", h.payloadLength)
		c.writeError(IANAStatusCodeProtocolError, err)
		return err
	}

	if !h.framefin {
		err := errors.New("received fragmented control frame")
		c.writeError(IANAStatusCodeProtocolError, err)
		return err
	}

	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	b := c.readControlBuf[:h.payloadLength]
	_, err = c.readFramePayload(ctx, b)
	if err != nil {
		return err
	}

	if h.masked {
		maskRFC6455Sec53(h.maskKey, b)
	}

	switch h.opcode {
	case opPingFrame:
		return c.writeControl(ctx, opPongFrame, b)

	case opPongFrame:
		c.activePingsMu.Lock()
		pong, ok := c.actPings[string(b)]
		c.activePingsMu.Unlock()
		if ok {
			select {
			case pong <- struct{}{}:
			default:
			}
		}

		return nil
	}

	defer func() {
		c.readCloseFrameErr = err
	}()

	closeErr, err := parseClosePayload(b)
	if err != nil {
		err = fmt.Errorf("received invalid close payload: %w", err)
		c.writeError(IANAStatusCodeProtocolError, err)

		return err
	}

	err = fmt.Errorf("received close frame: %w", closeErr)
	c.setCloseErr(err)
	c.writeClose(closeErr.Code, closeErr.Msg)
	c.close(err)

	return err
}

func (c *Connector) reader(ctx context.Context) (_ MessageType, _ io.Reader, err error) {
	defer werr.ErrWrap(&err, "failed to get reader")

	err = c.readMu.lock(ctx)
	if err != nil {
		return 0, nil, err
	}
	defer c.readMu.unlock()

	if !c.msgReader.fin {
		err = errors.New("previous message not read to completion")
		c.close(fmt.Errorf("failed to get reader: %w", err))

		return 0, nil, err
	}

	h, err := c.readLoop(ctx)
	if err != nil {
		return 0, nil, err
	}

	if h.opcode == opContinuationFrame {
		err := errors.New("received continuation frame without text or binary frame")
		c.writeError(IANAStatusCodeProtocolError, err)

		return 0, nil, err
	}

	c.msgReader.reset(ctx, h)

	return MessageType(h.opcode), c.msgReader, nil
}

type readerFunc func(bt []byte) (int, error)

func (f readerFunc) Read(bt []byte) (int, error) {
	return f(bt)
}
