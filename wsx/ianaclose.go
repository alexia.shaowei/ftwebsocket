package ftwebsocket

import (
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"log"
	"time"

	werr "sw.websocket/wsx/internal/wraperror"
)

// https://www.iana.org/assignments/websocket/websocket.xhtml#close-code-number
const (
	IANAStatusCodeNormalClosure           StatusCode = 1000
	IANAStatusCodeGoingAway               StatusCode = 1001
	IANAStatusCodeProtocolError           StatusCode = 1002
	IANAStatusCodeUnsupportedData         StatusCode = 1003
	IANAStatusCodeReserved                StatusCode = 1004
	IANAStatusCodeNoStatusRcvd            StatusCode = 1005
	IANAStatusCodeAbnormalClosure         StatusCode = 1006
	IANAStatusCodeInvalidFramePayloadData StatusCode = 1007
	IANAStatusCodePolicyViolation         StatusCode = 1008
	IANAStatusCodeMessageTooBig           StatusCode = 1009
	IANAStatusCodeMandatoryExtension      StatusCode = 1010
	IANAStatusCodeInternalError           StatusCode = 1011
	IANAStatusCodeServiceRestart          StatusCode = 1012
	IANAStatusCodeTryAgainLater           StatusCode = 1013
	IANAStatusCodeBadGateway              StatusCode = 1014
	IANAStatusCodeTLSHandshake            StatusCode = 1015
)

// rfc6455#section-7.4
type StatusCode int

type CloseError struct {
	Code StatusCode
	Msg  string
}

func (ref CloseError) Error() string {
	return fmt.Sprintf("status = %v and reason = %q", ref.Code, ref.Msg)
}

func CloseStatus(err error) StatusCode {
	var closeErr CloseError
	if errors.As(err, &closeErr) {
		return closeErr.Code
	}

	return -1
}

func (ref *Connector) Close(code StatusCode, msg string) error {
	return ref.closeHandshake(code, msg)
}

func (ref *Connector) closeHandshake(code StatusCode, msg string) (err error) {
	defer werr.ErrWrap(&err, "failed to close WebSocket")

	writeErr := ref.writeClose(code, msg)
	closeHandshakeErr := ref.waitCloseHandshake()

	if writeErr != nil {
		return writeErr
	}

	if CloseStatus(closeHandshakeErr) == -1 {
		return closeHandshakeErr
	}

	return nil
}

var errWrite = errors.New("wrote close")

func (ref *Connector) writeClose(code StatusCode, msg string) error {

	ref.closeMu.Lock()
	wroteClose := ref.wroteClose
	ref.wroteClose = true
	ref.closeMu.Unlock()
	if wroteClose {
		return errWrite
	}

	ce := CloseError{
		Code: code,
		Msg:  msg,
	}

	var p []byte
	var marshalErr error
	if ce.Code != IANAStatusCodeNoStatusRcvd {
		p, marshalErr = ce.bytes()
		if marshalErr != nil {
			log.Printf("websocket: %v", marshalErr)
		}
	}

	writeErr := ref.writeControl(context.Background(), opCloseFrame, p)
	if CloseStatus(writeErr) != -1 {
		writeErr = nil
	}

	ref.setCloseErr(fmt.Errorf("sent close frame: %w", ce))

	if marshalErr != nil {
		return marshalErr
	}
	return writeErr
}

func (ref *Connector) waitCloseHandshake() error {
	defer ref.close(nil)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	err := ref.readMu.lock(ctx)
	if err != nil {
		return err
	}
	defer ref.readMu.unlock()

	if ref.readCloseFrameErr != nil {
		return ref.readCloseFrameErr
	}

	for {
		h, err := ref.readLoop(ctx)
		if err != nil {
			return err
		}

		for i := int64(0); i < h.payloadLength; i++ {
			_, err := ref.bufioR.ReadByte()
			if err != nil {
				return err
			}
		}
	}
}

func parseClosePayload(payload []byte) (CloseError, error) {

	if len(payload) == 0 {
		return CloseError{
			Code: IANAStatusCodeNoStatusRcvd,
		}, nil
	}

	if len(payload) < 2 {
		return CloseError{}, fmt.Errorf("close payload %q too small, cannot even contain the 2 byte status code", payload)
	}

	closeErr := CloseError{
		Code: StatusCode(binary.BigEndian.Uint16(payload)),
		Msg:  string(payload[2:]),
	}

	if !validWireCloseCode(closeErr.Code) {
		return CloseError{}, fmt.Errorf("invalid status code %v", closeErr.Code)
	}

	return closeErr, nil
}

// http://www.iana.org/assignments/websocket/websocket.xhtml#close-code-number
// fc6455#section-7.4.1
func validWireCloseCode(code StatusCode) bool {

	switch code {
	case IANAStatusCodeReserved, IANAStatusCodeNoStatusRcvd, IANAStatusCodeAbnormalClosure, IANAStatusCodeTLSHandshake:
		return false
	}

	if code >= IANAStatusCodeNormalClosure && code <= IANAStatusCodeBadGateway {
		return true
	}

	if code >= 3000 && code <= 4999 {
		return true
	}

	return false
}

func (ref CloseError) bytes() ([]byte, error) {

	bt, err := ref.bytesErr()
	if err != nil {
		err = fmt.Errorf("failed to marshal close frame: %w", err)
		ref = CloseError{Code: IANAStatusCodeInternalError}

		bt, _ = ref.bytesErr()
	}

	return bt, err
}

const maxCloseMsg = maxPayload - 2

func (ref CloseError) bytesErr() ([]byte, error) {

	if len(ref.Msg) > maxCloseMsg {
		return nil, fmt.Errorf("reason string max is %v but got %q with length %v", maxCloseMsg, ref.Msg, len(ref.Msg))
	}

	if !validWireCloseCode(ref.Code) {
		return nil, fmt.Errorf("status code %v cannot be set", ref.Code)
	}

	buf := make([]byte, 2+len(ref.Msg))
	binary.BigEndian.PutUint16(buf, uint16(ref.Code))
	copy(buf[2:], ref.Msg)

	return buf, nil
}

func (ref *Connector) setCloseErr(err error) {

	ref.closeMu.Lock()
	ref.setCloseErrLocked(err)
	ref.closeMu.Unlock()
}

func (ref *Connector) setCloseErrLocked(err error) {

	if ref.closeErr == nil {
		ref.closeErr = fmt.Errorf("WebSocket closed: %w", err)
	}
}

func (ref *Connector) isClosed() bool {

	select {
	case <-ref.closed:
		return true

	default:
		return false
	}
}
