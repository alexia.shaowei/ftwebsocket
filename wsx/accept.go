package ftwebsocket

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	werr "sw.websocket/wsx/internal/wraperror"
)

type AcceptOptions struct {
	Subprotocols []string

	InsecureVerify       bool
	OriginPatterns       []string
	CompressionMode      CompressionMode
	CompressionThreshold int
}

func New(w http.ResponseWriter, r *http.Request, opts *AcceptOptions) (*Connector, error) {
	return new(w, r, opts)
}

func new(w http.ResponseWriter, r *http.Request, opts *AcceptOptions) (_ *Connector, err error) {
	defer werr.ErrWrap(&err, "failed to accept WebSocket connection")

	if opts == nil {
		opts = &AcceptOptions{}
	}

	errCode, err := verifyReq(w, r)
	if err != nil {
		http.Error(w, err.Error(), errCode)
		return nil, err
	}

	if !opts.InsecureVerify {
		err = authenticateOrigin(r, opts.OriginPatterns)
		if err != nil {
			if errors.Is(err, filepath.ErrBadPattern) {
				log.Printf("websocket: %v", err)
				err = errors.New(http.StatusText(http.StatusForbidden))
			}

			http.Error(w, err.Error(), http.StatusForbidden)

			return nil, err
		}
	}

	hijack, ok := w.(http.Hijacker)
	if !ok {
		err = errors.New("http.ResponseWriter no support http.Hijacker")
		http.Error(w, http.StatusText(http.StatusNotImplemented), http.StatusNotImplemented)

		return nil, err
	}

	w.Header().Set("Upgrade", "websocket")
	w.Header().Set("Connection", "Upgrade")

	key := r.Header.Get("Sec-WebSocket-Key")
	w.Header().Set("Sec-WebSocket-Accept", secWebSocketAccept(key))

	subproto := querySubprotocols(r, opts.Subprotocols)
	if subproto != "" {
		w.Header().Set("Sec-WebSocket-Protocol", subproto)
	}

	copts, err := acceptCompression(r, w, opts.CompressionMode)
	if err != nil {
		return nil, err
	}

	w.WriteHeader(http.StatusSwitchingProtocols)
	if gwt, ok := w.(interface{ WriteHeaderNow() }); ok {
		gwt.WriteHeaderNow()
	}

	netConnection, bufioRW, err := hijack.Hijack()
	if err != nil {
		err = fmt.Errorf("failed to hijack connection: %w", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return nil, err
	}

	bt, _ := bufioRW.Reader.Peek(bufioRW.Reader.Buffered())
	bufioRW.Reader.Reset(io.MultiReader(bytes.NewReader(bt), netConnection))

	return newConn(connCfg{
		subprotocol:     w.Header().Get("Sec-WebSocket-Protocol"),
		rwCloser:        netConnection,
		client:          false,
		compressionopts: copts,
		flateThreshold:  opts.CompressionThreshold,
		bufR:            bufioRW.Reader,
		bufW:            bufioRW.Writer,
	}), nil
}

func match(pattern, s string) (bool, error) {
	fmt.Println("accept.go:::match")
	return filepath.Match(strings.ToLower(pattern), strings.ToLower(s))
}
