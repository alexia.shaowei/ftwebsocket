package ftwebsocket

import (
	"context"
	"errors"
	"fmt"

	"github.com/klauspost/compress/flate"
	werr "sw.websocket/wsx/internal/wraperror"
)

type msgWriter struct {
	mw     *msgWriterState
	closed bool
}

func (mw *msgWriter) Write(p []byte) (int, error) {
	if mw.closed {
		return 0, errors.New("cannot use closed writer")
	}

	return mw.mw.Write(p)
}

func (mw *msgWriter) Close() error {
	if mw.closed {
		return errors.New("cannot use closed writer")
	}
	mw.closed = true

	return mw.mw.Close()
}

type msgWriterState struct {
	c          *Connector
	mu         *connMux
	writeMu    *connMux
	ctx        context.Context
	opcode     opcode
	flate      bool
	trimWriter *trimLastFourBytesWriter
	dict       slidingWindow
}

func newMsgWriterState(c *Connector) *msgWriterState {
	mw := &msgWriterState{
		c:       c,
		mu:      newConnMux(c),
		writeMu: newConnMux(c),
	}

	return mw
}

func (mw *msgWriterState) ensureFlate() {
	if mw.trimWriter == nil {
		mw.trimWriter = &trimLastFourBytesWriter{
			iow: writerFunc(mw.write),
		}
	}

	mw.dict.init(8192)
	mw.flate = true
}

func (mw *msgWriterState) flateContextTakeover() bool {
	if mw.c.client {
		return !mw.c.compressionopts.clientNoContextTakeover
	}

	return !mw.c.compressionopts.serverNoContextTakeover
}

func (mw *msgWriterState) Write(p []byte) (_ int, err error) {
	err = mw.writeMu.lock(mw.ctx)
	if err != nil {
		return 0, fmt.Errorf("failed to write: %w", err)
	}
	defer mw.writeMu.unlock()

	defer func() {
		if err != nil {
			err = fmt.Errorf("failed to write: %w", err)
			mw.c.close(err)
		}
	}()

	if mw.c.flate() {
		if mw.opcode != opContinuationFrame && len(p) >= mw.c.flateThreshold {
			mw.ensureFlate()
		}
	}

	if mw.flate {
		err = flate.StatelessDeflate(mw.trimWriter, p, false, mw.dict.buf)
		if err != nil {
			return 0, err
		}
		mw.dict.write(p)

		return len(p), nil
	}

	return mw.write(p)
}

func (mw *msgWriterState) write(p []byte) (int, error) {
	n, err := mw.c.writeFrame(mw.ctx, false, mw.flate, mw.opcode, p)
	if err != nil {
		return n, fmt.Errorf("failed to write data frame: %w", err)
	}
	mw.opcode = opContinuationFrame

	return n, nil
}

func (mw *msgWriterState) reset(ctx context.Context, typ MessageType) error {
	err := mw.mu.lock(ctx)
	if err != nil {
		return err
	}

	mw.ctx = ctx
	mw.opcode = opcode(typ)
	mw.flate = false

	mw.trimWriter.reset()

	return nil
}

func (mw *msgWriterState) Close() (err error) {
	defer werr.ErrWrap(&err, "failed to close writer")

	err = mw.writeMu.lock(mw.ctx)
	if err != nil {
		return err
	}
	defer mw.writeMu.unlock()

	_, err = mw.c.writeFrame(mw.ctx, true, mw.flate, mw.opcode, nil)
	if err != nil {
		return fmt.Errorf("failed to write fin frame: %w", err)
	}

	if mw.flate && !mw.flateContextTakeover() {
		mw.dict.close()
	}
	mw.mu.unlock()

	return nil
}

func (mw *msgWriterState) close() {
	if mw.c.client {
		mw.c.writeFrameMu.fLock()
		putBufioWriter(mw.c.bufioW)
	}

	mw.writeMu.fLock()
	mw.dict.close()
}
