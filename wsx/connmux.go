package ftwebsocket

import (
	"context"
	"fmt"
)

type connMux struct {
	conn *Connector
	ch   chan struct{}
}

func newConnMux(ref *Connector) *connMux {
	return &connMux{
		conn: ref,
		ch:   make(chan struct{}, 1),
	}
}

func (ref *connMux) fLock() {
	ref.ch <- struct{}{}
}

func (ref *connMux) lock(ctx context.Context) error {
	select {
	case <-ref.conn.closed:
		return ref.conn.closeErr

	case <-ctx.Done():
		err := fmt.Errorf("failed to lock: %w", ctx.Err())
		ref.conn.close(err)

		return err

	case ref.ch <- struct{}{}:
		select {
		case <-ref.conn.closed:
			ref.unlock()

			return ref.conn.closeErr

		default:

		}

		return nil
	}
}

func (ref *connMux) unlock() {
	select {

	case <-ref.ch:

	default:

	}
}
