package ftwebsocket

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"strings"
	"sync"
)

type msgReader struct {
	c *Connector

	ctx         context.Context
	flate       bool
	flateReader io.Reader
	flateBufio  *bufio.Reader
	flateTail   strings.Reader
	limitReader *limitReader
	dict        slidingWindow

	fin           bool
	payloadLength int64
	maskKey       uint32

	readFunc readerFunc
}

func newMsgReader(c *Connector) *msgReader {
	mr := &msgReader{
		c:   c,
		fin: true,
	}

	mr.readFunc = mr.read
	mr.limitReader = newLimitReader(c, mr.readFunc, defaultReadLimit+1)

	return mr
}

func (ref *msgReader) resetFlate() {
	if ref.flateContextTakeover() {
		ref.dict.init(32768)
	}
	if ref.flateBufio == nil {
		ref.flateBufio = getBufioReader(ref.readFunc)
	}

	ref.flateReader = getFlateReader(ref.flateBufio, ref.dict.buf)
	ref.limitReader.ior = ref.flateReader
	ref.flateTail.Reset(deflateMsgTail)
}

func (ref *msgReader) putFlateReader() {
	if ref.flateReader != nil {
		putFlateReader(ref.flateReader)
		ref.flateReader = nil
	}
}

func (ref *msgReader) close() {
	ref.c.readMu.fLock()
	ref.putFlateReader()
	ref.dict.close()
	if ref.flateBufio != nil {
		putBufioReader(ref.flateBufio)
	}

	if ref.c.client {
		putBufioReader(ref.c.bufioR)
		ref.c.bufioR = nil
	}
}

func (ref *msgReader) flateContextTakeover() bool {
	if ref.c.client {
		return !ref.c.compressionopts.serverNoContextTakeover
	}

	return !ref.c.compressionopts.clientNoContextTakeover
}

func (ref *msgReader) reset(ctx context.Context, h header) {
	ref.ctx = ctx
	ref.flate = h.framersv1
	ref.limitReader.reset(ref.readFunc)

	if ref.flate {
		ref.resetFlate()
	}

	ref.setFrame(h)
}

func (ref *msgReader) setFrame(h header) {
	ref.fin = h.framefin
	ref.payloadLength = h.payloadLength
	ref.maskKey = h.maskKey
}

func (ref *msgReader) Read(p []byte) (n int, err error) {
	err = ref.c.readMu.lock(ref.ctx)
	if err != nil {
		return 0, fmt.Errorf("failed to read: %w", err)
	}
	defer ref.c.readMu.unlock()

	n, err = ref.limitReader.Read(p)
	if ref.flate && ref.flateContextTakeover() {
		p = p[:n]
		ref.dict.write(p)
	}

	if errors.Is(err, io.EOF) || errors.Is(err, io.ErrUnexpectedEOF) && ref.fin && ref.flate {
		ref.putFlateReader()
		return n, io.EOF
	}

	if err != nil {
		err = fmt.Errorf("failed to read: %w", err)
		ref.c.close(err)
	}

	return n, err
}

func (ref *msgReader) read(p []byte) (int, error) {
	for {
		if ref.payloadLength == 0 {
			if ref.fin {
				if ref.flate {
					return ref.flateTail.Read(p)
				}

				return 0, io.EOF
			}

			h, err := ref.c.readLoop(ref.ctx)
			if err != nil {
				return 0, err
			}

			if h.opcode != opContinuationFrame {
				err := errors.New("received new data message without finishing the previous message")
				ref.c.writeError(IANAStatusCodeProtocolError, err)
				return 0, err
			}
			ref.setFrame(h)

			continue
		}

		if int64(len(p)) > ref.payloadLength {
			p = p[:ref.payloadLength]
		}

		n, err := ref.c.readFramePayload(ref.ctx, p)
		if err != nil {
			return n, err
		}

		ref.payloadLength -= int64(n)

		if !ref.c.client {
			ref.maskKey = maskRFC6455Sec53(ref.maskKey, p)
		}

		return n, nil
	}
}

var bufioReaderPool sync.Pool

func getBufioReader(r io.Reader) *bufio.Reader {
	br, ok := bufioReaderPool.Get().(*bufio.Reader)
	if !ok {
		return bufio.NewReader(r)
	}

	br.Reset(r)
	return br
}

func putBufioReader(br *bufio.Reader) {
	bufioReaderPool.Put(br)
}
