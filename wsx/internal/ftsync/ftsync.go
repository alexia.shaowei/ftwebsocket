package ftsync

import (
	"sync/atomic"
)

type Int64 struct {
	i atomic.Value
}

func (val *Int64) Load() int64 {
	idx, _ := val.i.Load().(int64)

	return idx
}

func (val *Int64) Store(i int64) {
	val.i.Store(i)
}
