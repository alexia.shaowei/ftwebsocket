package errwrap

import (
	"fmt"
)

func ErrWrap(err *error, msg string, v ...interface{}) {
	fmt.Printf("wraperror::ErrWrap: %v\n", msg)
	if *err != nil {
		*err = fmt.Errorf(msg+": %w", append(v, *err)...)
	}
}
